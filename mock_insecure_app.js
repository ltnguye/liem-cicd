// eslint.detect-eval-with-expression
function dangerous_eval(myeval) {
  eval(myeval);
}

// eslint.detect-object-injection First occurrence
function handler(anyVal, userInput) {
  user[anyVal] = user[userInput[0]](userInput[1]);
}

// // eslint.detect-object-injection Second occurrence
// function otherHandler(anyVal, userInput) {
//   user[anyVal] = user[userInput[0]](userInput[1]);
// }

